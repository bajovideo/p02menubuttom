package com.example.appmenubutton

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.appmenubutton.AlumnoLista

class MiAdaptador(
    private val context: Context,
    private var listaAlumnos: List<AlumnoLista>,
    private val itemClickListener: (AlumnoLista) -> Unit
) : RecyclerView.Adapter<MiAdaptador.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val nombre: TextView = itemView.findViewById(R.id.txtAlumnoNombre)
        private val especialidad: TextView = itemView.findViewById(R.id.txtCarrera)
        private val matricula: TextView = itemView.findViewById(R.id.txtMatricula)
        private val foto: ImageView = itemView.findViewById(R.id.foto)

        fun bind(alumno: AlumnoLista, clickListener: (AlumnoLista) -> Unit) {
            nombre.text = alumno.nombre
            especialidad.text = alumno.especialidad
            matricula.text = alumno.matricula
            foto.setImageURI(Uri.parse(alumno.foto))

            itemView.setOnClickListener { clickListener(alumno) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.alumno_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val alumno = listaAlumnos[position]
        holder.bind(alumno, itemClickListener)
    }

    override fun getItemCount(): Int {
        return listaAlumnos.size
    }

    fun actualizarLista(nuevaLista: List<AlumnoLista>) {
        listaAlumnos = nuevaLista
        notifyDataSetChanged()
    }
}
