package com.example.appmenubutton

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appmenubutton.AlumnoLista
import com.example.appmenubutton.database.dbAlumnos
import com.google.android.material.floatingactionbutton.FloatingActionButton

class SalirFragment : Fragment() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var miAdaptador: MiAdaptador
    private lateinit var listaAlumnos: List<AlumnoLista>
    private lateinit var db: dbAlumnos

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_salir, container, false)

        recyclerView = view.findViewById(R.id.recId)
        recyclerView.layoutManager = LinearLayoutManager(requireContext())

        db = dbAlumnos(requireContext())
        db.openDataBase()
        listaAlumnos = db.leerTodos()
        db.close()

        miAdaptador = MiAdaptador(requireContext(), listaAlumnos) { alumno ->
            cambiarDBFragment(alumno)
        }
        recyclerView.adapter = miAdaptador

        val searchView = view.findViewById<SearchView>(R.id.searchView)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                val filteredList = listaAlumnos.filter { alumno ->
                    alumno.nombre.contains(newText ?: "", ignoreCase = true) ||
                            alumno.matricula.contains(newText ?: "", ignoreCase = true) ||
                            alumno.especialidad.contains(newText ?: "", ignoreCase = true)
                }
                miAdaptador.actualizarLista(filteredList)
                return true
            }
        })

        val agregarAlumno = view.findViewById<FloatingActionButton>(R.id.agregarAlumno)
        agregarAlumno.setOnClickListener {
            cambiarDBFragment(null)
        }

        return view
    }

    private fun cambiarDBFragment(alumno: AlumnoLista?) {
        val dbFragment = dbFragment().apply {
            arguments = Bundle().apply {
                putSerializable("mialumno", alumno)
            }
        }
        requireActivity().supportFragmentManager.beginTransaction()
            .replace(R.id.frmContenedor, dbFragment)
            .addToBackStack(null)
            .commit()
    }
}
