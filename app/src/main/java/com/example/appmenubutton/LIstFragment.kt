package com.example.appmenubutton

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.SearchView
import androidx.appcompat.app.AlertDialog

class ListFragment : Fragment() {
    private lateinit var listView: ListView
    private lateinit var arrayList: ArrayList<String>
    private lateinit var adapter: ArrayAdapter<String>
    private lateinit var searchView: SearchView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_l_ist, container, false)
        listView = view.findViewById(R.id.lstAlumnos)
        searchView = view.findViewById(R.id.searchView)

        // Lee los alumnos del Array-String los pone en la variable items
        val items = resources.getStringArray(R.array.alumnos)

        // Inicia el objeto arrayList y agrega TODOS los items
        arrayList = ArrayList()
        arrayList.addAll(items)

        // Inicia el Adaptador con estilo, y el arrayList
        adapter = ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, arrayList)

        // Asigna el adaptador al listView
        listView.adapter = adapter
        listView.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            var alumno: String = parent.getItemAtPosition(position).toString()
            val builder = AlertDialog.Builder(requireContext())
            builder.setTitle("Lista de Alumnos")
            builder.setMessage(position.toString() + ": " + alumno)
            builder.setPositiveButton("OK") { dialog, which ->
                // Acción cuando el usuario hace clic en el botón OK
            }
            builder.show()
        }

        // Configurar el SearchView para filtrar los elementos en tiempo real
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                adapter.filter.filter(newText)
                return false
            }
        })

        return view
    }
}
