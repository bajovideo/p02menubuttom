package com.example.appmenubutton

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

class HomeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        val imgAlumno: ImageView = view.findViewById(R.id.imgAlumno)
        val txtNombreAlumno: TextView = view.findViewById(R.id.txtNombreAlumno)
        val txtMateria: TextView = view.findViewById(R.id.txtMateria)
        val txtCarrera: TextView = view.findViewById(R.id.txtCarrera)

        return view
    }
}
