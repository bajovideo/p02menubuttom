package com.example.appmenubutton

import java.io.Serializable

data class AlumnoLista(
    val id: Int,
    val matricula: String,
    val nombre: String,
    val domicilio: String,
    val especialidad: String,
    val foto: String
) : Serializable
