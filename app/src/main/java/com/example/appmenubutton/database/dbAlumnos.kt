package com.example.appmenubutton.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.example.appmenubutton.AlumnoLista

class dbAlumnos(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    companion object {
        private const val DATABASE_NAME = "sistema.db"
        private const val DATABASE_VERSION = 2
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL("""
            CREATE TABLE ${DefinirTabla.Alumnos.TABLA} (
                ${DefinirTabla.Alumnos.ID} INTEGER PRIMARY KEY AUTOINCREMENT,
                ${DefinirTabla.Alumnos.MATRICULA} TEXT,
                ${DefinirTabla.Alumnos.NOMBRE} TEXT,
                ${DefinirTabla.Alumnos.DOMICILIO} TEXT,
                ${DefinirTabla.Alumnos.ESPECIALIDAD} TEXT,
                ${DefinirTabla.Alumnos.FOTO} TEXT
            )
        """)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE IF EXISTS ${DefinirTabla.Alumnos.TABLA}")
        onCreate(db)
    }

    fun openDataBase(): SQLiteDatabase {
        return this.writableDatabase
    }

    fun InsertarAlumno(alumno: Alumno): Long {
        val db = this.writableDatabase
        val valores = ContentValues().apply {
            put(DefinirTabla.Alumnos.MATRICULA, alumno.matricula)
            put(DefinirTabla.Alumnos.NOMBRE, alumno.nombre)
            put(DefinirTabla.Alumnos.DOMICILIO, alumno.domicilio)
            put(DefinirTabla.Alumnos.ESPECIALIDAD, alumno.especialidad)
            put(DefinirTabla.Alumnos.FOTO, alumno.foto)
        }
        return db.insert(DefinirTabla.Alumnos.TABLA, null, valores)
    }

    fun ActualizarAlumno(alumno: Alumno): Int {
        val db = this.writableDatabase
        val valores = ContentValues().apply {
            put(DefinirTabla.Alumnos.MATRICULA, alumno.matricula)
            put(DefinirTabla.Alumnos.NOMBRE, alumno.nombre)
            put(DefinirTabla.Alumnos.DOMICILIO, alumno.domicilio)
            put(DefinirTabla.Alumnos.ESPECIALIDAD, alumno.especialidad)
            put(DefinirTabla.Alumnos.FOTO, alumno.foto)
        }
        val selection = "matricula = ?"
        val selectionArgs = arrayOf(alumno.matricula)
        return db.update(DefinirTabla.Alumnos.TABLA, valores, selection, selectionArgs)
    }

    fun BorrarAlumno(matricula: String): Int {
        val db = this.writableDatabase
        val selection = "matricula = ?"
        val selectionArgs = arrayOf(matricula)
        return db.delete(DefinirTabla.Alumnos.TABLA, selection, selectionArgs)
    }

    fun BuscarAlumno(matricula: String): Alumno? {
        val db = this.readableDatabase
        val cursor = db.query(
            DefinirTabla.Alumnos.TABLA,
            arrayOf(
                DefinirTabla.Alumnos.ID,
                DefinirTabla.Alumnos.MATRICULA,
                DefinirTabla.Alumnos.NOMBRE,
                DefinirTabla.Alumnos.DOMICILIO,
                DefinirTabla.Alumnos.ESPECIALIDAD,
                DefinirTabla.Alumnos.FOTO
            ),
            "matricula = ?",
            arrayOf(matricula),
            null, null, null
        )

        return if (cursor.moveToFirst()) {
            val alumno = Alumno(
                id = cursor.getInt(cursor.getColumnIndexOrThrow(DefinirTabla.Alumnos.ID)),
                matricula = cursor.getString(cursor.getColumnIndexOrThrow(DefinirTabla.Alumnos.MATRICULA)),
                nombre = cursor.getString(cursor.getColumnIndexOrThrow(DefinirTabla.Alumnos.NOMBRE)),
                domicilio = cursor.getString(cursor.getColumnIndexOrThrow(DefinirTabla.Alumnos.DOMICILIO)),
                especialidad = cursor.getString(cursor.getColumnIndexOrThrow(DefinirTabla.Alumnos.ESPECIALIDAD)),
                foto = cursor.getString(cursor.getColumnIndexOrThrow(DefinirTabla.Alumnos.FOTO))
            )
            cursor.close()
            alumno
        } else {
            cursor.close()
            null
        }
    }

    fun leerTodos(): List<AlumnoLista> {
        val db = this.readableDatabase
        val cursor = db.query(
            DefinirTabla.Alumnos.TABLA,
            arrayOf(
                DefinirTabla.Alumnos.ID,
                DefinirTabla.Alumnos.MATRICULA,
                DefinirTabla.Alumnos.NOMBRE,
                DefinirTabla.Alumnos.DOMICILIO,
                DefinirTabla.Alumnos.ESPECIALIDAD,
                DefinirTabla.Alumnos.FOTO
            ),
            null, null, null, null, null
        )

        val alumnos = mutableListOf<AlumnoLista>()
        if (cursor.moveToFirst()) {
            do {
                val alumno = AlumnoLista(
                    id = cursor.getInt(cursor.getColumnIndexOrThrow(DefinirTabla.Alumnos.ID)),
                    matricula = cursor.getString(cursor.getColumnIndexOrThrow(DefinirTabla.Alumnos.MATRICULA)),
                    nombre = cursor.getString(cursor.getColumnIndexOrThrow(DefinirTabla.Alumnos.NOMBRE)),
                    domicilio = cursor.getString(cursor.getColumnIndexOrThrow(DefinirTabla.Alumnos.DOMICILIO)),
                    especialidad = cursor.getString(cursor.getColumnIndexOrThrow(DefinirTabla.Alumnos.ESPECIALIDAD)),
                    foto = cursor.getString(cursor.getColumnIndexOrThrow(DefinirTabla.Alumnos.FOTO))
                )
                alumnos.add(alumno)
            } while (cursor.moveToNext())
        }
        cursor.close()
        return alumnos
    }
}
